class MyBigNumber {
  sum(stn1, stn2) {
    let result = "";
    let carry = 0;
    let stn1Length = stn1.length;
    let stn2Length = stn2.length;

    let maxLength = Math.max(stn1Length, stn2Length);
    for (let i = 1; i <= maxLength; i++) {
      let a = +stn1.charAt(stn1Length - i);
      let b = +stn2.charAt(stn2Length - i);

      let sum = carry + a + b;

      carry = (sum / 10) | 0;

      sum %= 10;

      result =
        i === maxLength && carry ? carry * 10 + sum + result : sum + result;
    }
    return result;
  }
}
const myBigNumber = new MyBigNumber();

module.exports = myBigNumber;
